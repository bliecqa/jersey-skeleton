package fr.iutinfo.skeleton.api;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.IOException;

public class UploadTest extends JerseyTest {
    final static Logger logger = LoggerFactory.getLogger(UploadTest.class);

    @Override
    protected Application configure() {
        return new Api();
    }

    @Override
    protected void configureClient(ClientConfig config) {
        config.register(MultiPartFeature.class);
    }

    @Test
    public void should_upload_file() throws IOException {
        int statusCode = upload("file content").getStatus();
        Assert.assertEquals(200, statusCode);
    }

    @Test
    public void uploaded_files_could_be_found_on_server() throws IOException {
        String expectedContent = "Content of file to upload";
        String fileName = upload(expectedContent).readEntity(String.class);
        String path = System.getProperty("java.io.tmpdir") + System.getProperty("file.separator") + fileName;
        logger.debug("File to test : " + path);
        Assert.assertEquals(expectedContent, Files.toString(new File(path), Charsets.UTF_8));
    }

    private Response upload(String fileContent) throws IOException {
        File fileToUpload = File.createTempFile("upload", ".dat");
        Files.write(fileContent, fileToUpload, Charsets.UTF_8);
        logger.debug("Upload file : " + fileToUpload.getPath());
        FileDataBodyPart filePart = new FileDataBodyPart("file", fileToUpload);
        MultiPart multipart = new FormDataMultiPart()
                .field("name", "value")
                .bodyPart(filePart);
        return target("upload")
                .request()
                .post(Entity.entity(multipart, multipart.getMediaType()));
    }
}
