package fr.iutinfo.skeleton.auth;

import org.glassfish.jersey.internal.util.Base64;

public class BasicAuth {
    public static String[] decode(String auth) {

        String decodedAuth = Base64.decodeAsString(extractBase64(auth));
        if (decodedAuth == null || decodedAuth.isEmpty() ) {
            return null;
        }

        return decodedAuth.split(":", 2);
    }

    private static String extractBase64(String auth) {
        return auth.replaceFirst("[B|b]asic ", "");
    }
}
